#include<iostream>
#include <stdlib.h> 
#include<time.h>
#include<vector>
#include<algorithm>

using namespace std;
bool comp(const int& first_num, const int& second_num);
void testmovmin();
bool task3(vector <int>&t1, vector<int> &t2);
bool movMin(vector<int> &in, vector<int> &out);
int main()
{
	testmovmin(); //calling test function to test two sorted functions.
		getchar();
	getchar();
	return 0;
}

// taking 2 vectors as parameters and checking whether they are equal or not
bool movMin(vector<int> &in, vector<int> &out){
	int i, j;
	int temp;
	for (i = 1; i < in.size(); i++)
	{
		for (j = 0; j < in.size(); j++) //loop for sorting numbers stored in vector
		{
			if (in[j] > in[i])
			{
				temp = in[j];
				in[j] = in[i];
				in[i] = temp;
			}
		}
	}

	if (in == out){ //condition to check whether two sorted vectors are equal or not
		return true;
	}
	else{
		return false;
	}
}
// to make sure that sort function is sorting in ascending or descending order 
bool comp(const int& first_num, const int& second_num){
	return first_num < second_num;
}


void testmovmin()
{

	vector<int> in;

	int max = 0;
	srand(time(NULL));
	vector<float> time;
	vector<float> times;
	for (int k = 10; k <= 1000000; k * 10){ //calculating runtime for different inputs

		int num = rand() % 20 + 1; //generating random number between 1-20 for loop 

		for (int i = 0; i < num; i++)
		{
			int r = rand() % 100 + 1;
			in.push_back(r);       //loop to store random number in vector and finding max value in vector.
			if (r>max){
				max = r;
			}
		}
		max = max - 2;
		int lastind = rand() % max + 1; //generating number less than max number or last number in vector
		in.push_back(lastind); //storing random number in vector
		vector<int> out = in; //storing vector in numbers to vector out

		sort(out.begin(), out.end(), comp); //sorting vector by sort function
		const clock_t begin_time = clock();
		movMin(in, out);
		float ta = float(clock() - begin_time) / CLOCKS_PER_SEC;
		time.push_back(ta); // storing vaue of runtime.
		const clock_t begin_time2 = clock();
		task3(in, out);
		float tb = float(clock() - begin_time2) / CLOCKS_PER_SEC;
		times.push_back(tb);
	}
	sort(time.begin(), time.end(), comp);
	sort(times.begin(), times.end(), comp);
	cout << "Minimum time: " << time[0] << endl;    //displaying minimum runtime for single for loop
	cout << "Minimum time for nested loops " << times[0] << endl; //displaying minimum runtime for nested loop
	cout << "Maximum time: " << time[time.size() - 1] << endl; // displaying maximum runtime for single loop
	cout << "Maximum time for nested loops " << times[times.size() - 1] << endl; //displaying maximum runtime for nested for loop
}

//task#3
bool task3(vector <int>&t1, vector<int> &t2) //sorting vector in single for loop
{
	int temp1;
	int j = 0;
	int v = t1.size() - 1;
	for (int i = 0; i<t1.size() - 1; i++) //sorting vector
	{
		if (t1[v]<t1[i]){
			t1.insert(t1.begin() + i, t1[v]);
			t1.pop_back();
		}
	}
	if (t1 == t2){ //checking whether two sorted vectors are equal or not
		return true;
	}
	return false;
}
